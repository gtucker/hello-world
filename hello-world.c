/*
    hello-world.c

    Copyright (C) 2020 Guillaume Tucker <guillaume.tucker@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define BUFFER_SZ 64

static const char TIME_FMT[] = "%Y-%m-%e %H:%M:%S %z";
static const unsigned int MAX_SLEEP_S = 15;

int main(void) {
	time_t now;
	struct tm now_cal;
	char now_str[BUFFER_SZ];

	srand(time(NULL));

	for (;;) {
		printf("- Hello World! How's things?\n");

		now = time(NULL);
		if (now == (time_t)-1) {
			fprintf(stderr, "Failed to get current time\n");
			exit(EXIT_FAILURE);
		}

		if (localtime_r(&now, &now_cal) == NULL) {
			fprintf(stderr, "Failed to convert to calendar time\n");
			exit(EXIT_FAILURE);
		}

		if (!strftime(now_str, BUFFER_SZ, TIME_FMT, &now_cal)) {
			fprintf(stderr, "Failed to format date and time\n");
			exit(EXIT_FAILURE);
		}

		printf("- Still spinning, time is now: %s\n...\n", now_str);

		sleep(1 + ((float)rand() / RAND_MAX) * MAX_SLEEP_S);
	}

	return 0;
}
